<?php

namespace App\Controller;

use App\Entity\Messages;
use App\Entity\Rooms;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class RoomsController extends Controller
{
    public function __construct() {
        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer();

        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });

        $this->serializer = new Serializer(array($normalizer), array($encoder));
    }

    /**
     * @Route("/room")
     * @Method({"GET","PUT","OPTIONS"})
     */
    public function index()
    {

        $rooms = $this->getDoctrine()->getRepository(Rooms::class)->findAll();
        $json = $this->serializer->serialize($rooms, 'json');
        return new Response($json);
    }

    /**
     * @Route("/room/new")
     * @Method({"OPTIONS", "POST"})
     */
    public function new(Request $request)
    {
        if(!empty($request->getContent())) {
            $entityManager = $this->getDoctrine()->getManager();
            $data = json_decode($request->getContent(), true);

            $room = new Rooms();
            $room->setName($data['name']);

            $entityManager->persist($room);

            $entityManager->flush();

            $data = ['room_name' => $room->getName(), 'room_id' => $room->getId()];

            return new JsonResponse($data);
        }
        return new JsonResponse(['message' => 'nani']);
    }

    /**
     * @Route("/room/{id}")
     * @Method({"GET"})
     */
    public function show($id)
    {
        $doctrine = $this->getDoctrine();
        $room = $doctrine->getRepository(Rooms::class)->findOneBy(['id' => $id]);
        $message = $doctrine->getRepository(Messages::class)->findByRoom($id);

        $data =[];
        $data['room'] = $room;
        $data['messages'] = $message;

        $json = $this->serializer->serialize($data, 'json');

        return new Response($json);
    }

    /**
     * @Route("/room/search/{name}")
     * @Method({"GET"})
     */
    public function search($name)
    {
        $rooms = $this->getDoctrine()->getRepository(Rooms::class)->Search($name);

        if(empty($rooms)) {
            return new JsonResponse(['message' => 'Aucun résultats trouvés']);
        }

        return new JsonResponse($rooms);
    }
}
