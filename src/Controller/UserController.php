<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class UserController extends Controller
{
    /**
     * @Route("/user")
     */
    public function index()
    {
        return $this->render('user/index.html.twig', [
            'controller_name' => 'UserController',
        ]);
    }

    /**
     * @Route("/user/new")
     * @Method({"OPTIONS", "POST"})
     */
    public function new(Request $request)
    {
        if(!empty($request->getContent())) {
            $entityManager = $this->getDoctrine()->getManager();
            $data = json_decode($request->getContent(), true);

            $users = $this->getDoctrine()->getRepository(User::class)->findAll();

            foreach ($users as $value) {
                if ($value->getPseudo() === $data['pseudo']) {
                    return new JsonResponse(['message' => 'Error']);
                }
            }

            $user = new User();
            $user->setPseudo($data['pseudo']);
            $user->setPassword($data['password']);

            $entityManager->persist($user);
            $entityManager->flush();

            if ($user->getId() != NULL) {
                $data = ['message' => 'Compte créer avec succés'];
            } else {
                $data = ['message' => 'Error'];
            }

            return new JsonResponse($data);
        }
        return new JsonResponse(['']);
    }

    /**
     * @Route("/user/connect")
     * @Method({"OPTIONS", "POST"})
     */
    public function connect(Request $request){
        if(!empty($request->getContent())) {
            $data = json_decode($request->getContent(), true);

            $user = $this->getDoctrine()->getRepository(User::class)->findOneBy([
                'pseudo' => $data['pseudo'],
                'password' => $data['password']
            ]);
        }
        if(empty($request->getContent()) || $user === NULL) {
            return new JsonResponse(['response' => false]);
        } else {
            return new JsonResponse(['response' => true, 'user_id' => $user->getId(), 'user_name' => $user->getPseudo()]);
        }

    }
}
