<?php

namespace App\Controller;

use App\Entity\Messages;
use App\Entity\Rooms;
use App\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MessagesController extends Controller
{
    /**
     * @Route("/{room_id}/message")
     * @Method({"GET"})
     */
    public function message($room_id)
    {
        $messages = $this->getDoctrine()->getRepository(Messages::class)->findByRoom($room_id);

        return new JsonResponse($messages);
    }

    /**
     * @Route("/{room_id}/message/new")
     * @Method({"OPTIONS", "POST"})
     */
    public function new(Request $request,$room_id) {
        if(!empty($request->getContent())) {
            $data = json_decode($request->getContent(), true);

            $em = $this->getDoctrine()->getManager();

            $room = $this->getDoctrine()->getRepository(Rooms::class)->findOneBy(['id' => $room_id]);

            $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['id' => $data['user_id']]);

            $newmessage = new Messages();
            $newmessage->setDate(new \DateTime());
            $newmessage->setMessage($data['message']);
            $newmessage->setUser($user);
            $newmessage->setRoom($room);

            $em->persist($newmessage);

            $em->flush();

            $message = $this->getDoctrine()->getRepository(Messages::class)->findByRoom($room_id);

            return new JsonResponse($message);
        }
        return new JsonResponse(['message' => 'next']);
    }

    /**
     * @Route("/message/{id}")
     * @Method({"OPTIONS", "DELETE"})
     */
    public function delete($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $message = $this->getDoctrine()->getRepository(Messages::class)->findOneBy(['id' => $id]);

        $entityManager->remove($message);
        $entityManager->flush();

        return new JsonResponse(['message' => 'DONE']);
    }
}
