# Routing

Route disponible : 

- Création utilisateur
	user/new		POST
- Connexion
	user/connect	POST(return success + name user)
- Création
	room/new		POST
- Affichage des room
	room		GET
- Affichage de la room
	room/{id}		GET
- Recherche room
	room/search/{name}	GET
- Affichage messages en fonction de la room
	{room_id}/message	GET
- Ajout de message en fonction de la room 
	{room_id}/message/new	POST(return all messages from this room)
- Suppression de message en fonction de la room
	message/{id}		DELETE

